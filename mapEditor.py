#!/usr/bin/env python3
# -*- coding: iso-8859-1; -*-

# Original code by Erik Hansson 2014
# Link�pings University

from res.botraceGraphics import Button, TILE_COLOR
from res.graphics import Text, Point, Line, Rectangle, Circle, GraphWin, Entry

import os

TEXT_SIZE = 16
SAVE_MAP = "Botrace_Maps"

class StartPosition():

    def __init__(self, p1, window):

        self.window = window

        self.position = p1
        self.directions_nr = 0
        self.directions = []
        self.directions.append(Line(Point(p1.getX(), p1.getY()+0.5),
                                   Point(p1.getX(), p1.getY()-0.5)))
        self.directions.append(Line(Point(p1.getX()+0.5, p1.getY()),
                                   Point(p1.getX()-0.5, p1.getY())))
        self.directions.append(Line(Point(p1.getX(), p1.getY()-0.5),
                                   Point(p1.getX(), p1.getY()+0.5)))
        self.directions.append(Line(Point(p1.getX()-0.5, p1.getY()),
                                   Point(p1.getX()+0.5, p1.getY())))
        for direction in self.directions:
            direction.setArrow("first")
        self.background = Circle(p1, 0.5)
        self.background.setFill("green")

    def draw(self):
        """
        Draws the start position to the window
        """
        self.background.draw(self.window)
        self.directions[self.directions_nr].draw(window)

    def undraw(self):
        """
        Undraws the start position
        """
        self.background.undraw()
        self.directions[self.directions_nr].undraw()

    def move(self, p1, direction_i):
        """
        moves the start position to the given position
        """
        self.directions[self.directions_nr].undraw()
        self.directions_nr = direction_i
        self.directions[self.directions_nr].draw(self.window)

        x_diff = p1.getX()-self.position.getX()
        y_diff = p1.getY()-self.position.getY()

        self.background.move(x_diff, y_diff)
        for direction in self.directions:
            direction.move(x_diff, y_diff)

        self.position = p1

    def set_direction(self, direction_i):
        """
        Sets the current direction to direction_i
        """
        self.directions[self.direction_nr].undraw()
        self.direction_nr = direction_i
        self.directions[self.direction_nr].draw(self.window)

    def get_position():
        """
        Gets which coords, in map coordinates, the start position has
        """
        return (int(self.p1.getX()-3), int(self.p2.getY()-3))

    def get_direction():
        """
        Gets the current direction of the start position
        """
        return self.direction_nr

class MapTile():

    def __init__(self, p1, p2):

        self.tile_type = []
        self.p1 = p1
        self.p2 = p2

        self.text = Text(Point((p1.getX()+p2.getX())/2,
                         (p1.getY()+p2.getY())/2),
                         "")
        self.text.setSize(12)
        self.text.setTextColor("white")

        self.tile = Rectangle(p1, p2)
        self.tile.setFill(TILE_COLOR["open"])

    def draw(self, window):
        """
        Draws the tile to the window
        """
        self.tile.draw(window)
        self.text.draw(window)
        self.window = window

    def set_blocked(self):
        """
        Sets the tile to blocked
        """
        self.tile_type = [-1]
        self.tile.setFill(TILE_COLOR["blocked"])
        self.window.flush()

    def set_open(self):
        """
        Sets the tile to open
        """
        self.tile_type = []
        self.tile.setFill(TILE_COLOR["open"])
        self.window.flush()

    def _update_text(self):
        """
        Updates the text of the tile
        """
        text = ""
        for i in range(len(self.tile_type)):
            if i >= 2:
                text += "..."
                break
            elif i == len(self.tile_type)-1:
                text += str(self.tile_type[i])
            else:
                text += str(self.tile_type[i]) + ", "
        self.text.setText(text)

    def set_destination(self, nr):
        """
        Sets the tile to destination nr
        """
        self.tile_type.append(nr)

        self.tile_type.sort()

        self._update_text()

        self.tile.setFill(TILE_COLOR["destination"])

        self.window.flush()

    def reset_tile(self):
        """
        Resets the tile
        """
        self.tile_type = []
        self._update_text()
        self.tile.setFill(TILE_COLOR["open"])
        self.window.flush()

def confirm_window(text):
    """
    Opens a confirmation window
    """
    window = GraphWin("Bekr�fta", 240, 120, autoflush=False)
    window.setCoords(0, 0, 6, 4)

    text = Text(Point(3, 3), text)
    text.setSize(TEXT_SIZE)
    text.draw(window)

    confirm_button = Button(Point(0.5, 0.5), Point(2.5, 1.5), "Ja")
    confirm_button.draw(window)

    cancel_button = Button(Point(3.5, 0.5), Point(5.5, 1.5), "Nej")
    cancel_button.draw(window)

    result = False
    while True:
        mouse_click = window.getMouse()

        if confirm_button.is_clicked(mouse_click):
            result = True
            break
        elif cancel_button.is_clicked(mouse_click):
            break

    # close window
    window.close()
    return result

def get_save_file_name():
    """
    Opens a pop-up for requesting a file name
    """
    window = GraphWin("Spara karta", 240, 160, autoflush=False)
    window.setCoords(0, 0, 6, 4)

    text = Text(Point(2, 3.25), "Skriv in filnamet")
    text.setSize(TEXT_SIZE)
    text.draw(window)

    entry = Entry(Point(2.7, 2.5), 20)
    entry.setSize(TEXT_SIZE)
    entry.draw(window)

    save_button = Button(Point(0.5, 0.5), Point(2.5, 1.5), "Spara")
    save_button.draw(window)

    cancel_button = Button(Point(3.5, 0.5), Point(5.5, 1.5), "Avbryt")
    cancel_button.draw(window)

    save_name = ""

    while True:
        mouse_click = window.getMouse()

        if save_button.is_clicked(mouse_click):
            save_name = entry.getText()
            if save_name:
                save_name += ".py"
                if os.path.exists(SAVE_MAP + "/" + save_name) and\
                   not confirm_window("Filen existerar redan.\n" +
                                      "Vill du spara �ver den?"):
                    save_name = ""
                else:
                    break
            else:
                entry.setFill("red")
        elif cancel_button.is_clicked(mouse_click):
            break

    window.close()
    return save_name

def save(tiles, start_position, direction, file_name):
    """
    Saves the map to the given file name
    """
    result = True
    _map = {
        "length" : 0,
        "map" : [],
        "direction" : direction
    }

    if start_position and start_position[0] != -1:
        _map["start"] = start_position
    else:
        _map["start"] = [0, 0]

    needed_checkpoints = []
    for x in range(len(tiles)):
        _map["map"].append([])
        for y in range(len(tiles[x])):
            _map["map"][x].append(tiles[x][y].tile_type)
            for checkpoint in tiles[x][y].tile_type:
                if checkpoint != -1:
                    if checkpoint > _map["length"]:
                        for i in range(checkpoint-_map["length"]):
                            needed_checkpoints.append(False)
                            _map["length"] = checkpoint
                    needed_checkpoints[checkpoint-1] = True

    for checkpoint in needed_checkpoints:
        if not checkpoint:
            result = False

    if result:
        f = open(SAVE_MAP + "/" + file_name, "w", encoding="latin-1")
        f.write("# Autogenerated save file\n")
        f.write("botrace_map = " + str(_map) + "\n")
        f.close()

    return result

def information_window(text):
    """
    Displaying an information window
    """

    # Dividing the text into rows of 60 chars
    texts = []
    while (len(text) > 60):
        try:
            index = text.rindex(" ", 0, 60)
            text = text[:index] + text[index+1:]
        except ValueError:
            index = 60
            text = text[:index] + "-" + text[index:]
        texts.append(text[:index])
        text = text[index:]
    texts.append(text)

    window = GraphWin("Information", 400, 60+len(texts)*30, autoflush=False)
    window.setCoords(0, 0, 10, 2+len(texts))

    for i in range(len(texts)):
        textArea = Text(Point(5, 2.5+0.5*i), texts[len(texts)-1-i])
        textArea.setSize(TEXT_SIZE)
        textArea.draw(window)

    ok_button = Button(Point(4, 0.5), Point(6, 1.5), "OK")
    ok_button.draw(window)

    while not ok_button.is_clicked(window.getMouse()):
        pass
    window.close()

def set_destination(tiles, start_position, x, y, direction,
                    start_position_mesh):
    """
    Sets the given position to a destination
    """

    tile_type = tiles[x][y].tile_type
    if (tile_type and tile_type[0] >= 0) or not tile_type:

        window = GraphWin("Checkpoint nummer", 240, 160, autoflush=False)
        window.setCoords(0, 0, 6, 4)

        text = Text(Point(3, 3.25), "Skriv in checkpointens nummer")
        text.setSize(TEXT_SIZE)
        text.draw(window)

        entry = Entry(Point(2.7, 2.5), 2)
        entry.setSize(TEXT_SIZE)
        entry.draw(window)

        confirm_button = Button(Point(0.5, 0.5), Point(2.5, 1.5), "OK")
        confirm_button.draw(window)

        cancel_button = Button(Point(3.5, 0.5), Point(5.5, 1.5), "Avbryt")
        cancel_button.draw(window)

        destination_nr = None

        while True:
            mouse_click = window.getMouse()

            if confirm_button.is_clicked(mouse_click):
                entry_text = entry.getText()
                if entry_text:
                    try:
                        destination_nr = int(entry_text)
                        if destination_nr < 1:
                            raise ValueError
                        break
                    except ValueError:
                        information_window("Enbart heltal st�rre �n 0 �r till�tna som "+
                                           "nummer f�r en Checkpoint")
                        entry.setText("")
                        entry_text = None
                else:
                    entry.setFill("red")
            elif cancel_button.is_clicked(mouse_click):
                break
        window.close()

        if isinstance(destination_nr, int) and destination_nr > 0:
            tiles[x][y].set_destination(destination_nr)
            return True
        else:
            return False
    else:
        return False

def set_blocked(tiles, start_position, x, y, direction, start_position_mesh):
    """
    Sets a tile to blocked
    """
    tile_type = tiles[x][y].tile_type
    result = False
    if not tile_type and not (start_position[0] == x and\
       start_position[1] == y):
        tiles[x][y].set_blocked()
        result = True
    return result

def get_direction(text):
    """
    Opens a direction windown and returns the direction
    """
    window = GraphWin("Riktningsf�rfr�gan", 240, 280, autoflush=False)
    window.setCoords(0, 0, 6, 7)

    text = Text(Point(3, 6), text)
    text.setSize(TEXT_SIZE)
    text.draw(window)

    up_button = Button(Point(2.25, 3.5), Point(3.75, 5.5), "Upp")
    up_button.draw(window)

    left_button = Button(Point(0.5, 2.5), Point(3, 3.5), "V�nster")
    left_button.draw(window)

    right_button = Button(Point(3, 2.5), Point(5.5, 3.5), "H�ger")
    right_button.draw(window)

    down_button = Button(Point(2.25, 0.5), Point(3.75, 2.5), "Ner")
    down_button.draw(window)

    result = None
    while True:
        mouse_click = window.getMouse()

        if up_button.is_clicked(mouse_click):
            result = 0
            break
        elif right_button.is_clicked(mouse_click):
            result = 1
            break
        elif down_button.is_clicked(mouse_click):
            result = 2
            break
        elif left_button.is_clicked(mouse_click):
            result = 3
            break
    window.close()

    return result

def set_start_position(tiles, start_position, x, y, direction,
                       start_position_mesh):
    """
    Sets the start position
    """
    tile_type = tiles[x][y].tile_type
    result = False
    if not tile_type or (tile_type and tile_type[0] >= 0):
        direction[0] = get_direction("Ange botens start riktning")
        start_position_mesh.move(Point(x+3.5, y+3.5), direction[0])
        try:
            start_position_mesh.undraw()
        except:
            pass
        start_position_mesh.draw()
        start_position[0] = x+1 # Botrace uses start index 1
        start_position[1] = y+1 # Botrace uses start index 1
        result = True
    return result

def delete(tiles, start_position, x, y, direction, start_position_mesh):
    """
    Deletes that is inside the square
    """
    tile_type = tiles[x][y].tile_type
    result = False
    if tile_type:
        tiles[x][y].reset_tile()
        result = True
    # Take Botrace start position of 1 into consideration
    if start_position[0]-1 == x and start_position[1]-1 == y:
        try:
            start_position_mesh.undraw()
        except:
            pass
        start_position[0] = -1
        start_position[1] = -1
        result = True
    return result

if __name__ == "__main__":


    ########### Draws all the graphics ##########
    window_title = "Botrace map editor"
    window = GraphWin(window_title, 960, 640, autoflush=False)
    window.setCoords(0, 0, 24, 16)

    tiles = []
    for x in range(10):
        tiles.append([])
        for y in range(10):
            tiles[x].append(MapTile(Point(x+3, y+3), Point(x+4, y+4)))
            tiles[x][y].draw(window)
    start_position_mesh = StartPosition(Point(1,1), window)

    save_button = Button(Point(15, 12), Point(19, 13), "Spara")
    save_button.draw(window)
    save_button.set_greyed_out(True)

    save_as_button = Button(Point(15, 10.5), Point(19, 11.5), "Spara som")
    save_as_button.draw(window)
    save_as_button.set_greyed_out(True)

    destination_button = Button(Point(15, 9), Point(19, 10), "Checkpoint")
    destination_button.draw(window)
    destination_example = MapTile(Point(20, 9), Point(21, 10))
    destination_example.draw(window)
    destination_example.set_destination(1)

    blocked_button = Button(Point(15, 7.5), Point(19, 8.5), "Blockera")
    blocked_button.draw(window)
    blocked_example = MapTile(Point(20, 7.5), Point(21, 8.5))
    blocked_example.draw(window)
    blocked_example.set_blocked()

    start_button = Button(Point(15, 6), Point(19, 7), "Start position")
    start_button.draw(window)
    start_example = StartPosition(Point(20.5, 6.5), window)
    start_example.draw()

    delete_button = Button(Point(15, 4.5), Point(19, 5.5), "Ta bort")
    delete_button.draw(window)

    exit_button = Button(Point(15, 3), Point(19, 4), "Avsluta")
    exit_button.draw(window)


    ########## Initiates all the neede variables ##########

    save_file_name = ""
    start_position = [-1, -1]
    current_tool = None
    current_button = None
    unsaved_changes = False
    direction = [0]

    if not os.path.isdir(SAVE_MAP):
        os.makedirs(SAVE_MAP)
    if not os.path.exists(SAVE_MAP + "/__init__.py"):
        open(SAVE_MAP + "/__init__.py", 'a').close()

    ########## Main Loop ##########
    while True:
        mouse_click = window.getMouse()

        if save_button.is_clicked(mouse_click) and unsaved_changes:
            if not save_file_name:
                save_file_name = get_save_file_name()
            if save_file_name:
                if not save(tiles, start_position, direction[0],
                            save_file_name):
                    information_window("Sparning misslyckades pga. saknad" +
                                       "checkpoint.\n" +
                                       "Det m�ste g� att komma till det " +
                                       "h�gsta nummret fr�n 1.")
                else:
                    unsaved_changes = False
                    save_button.set_greyed_out(True)
                    # [:-3] ignores the .py file ending
                    window.setTitle(window_title + " - " + save_file_name[:-3])

        elif save_as_button.is_clicked(mouse_click) and unsaved_changes:
            tmp = get_save_file_name()
            if tmp:
                save_file_name = tmp
                if not save(tiles, start_position, direction[0],
                            save_file_name):
                    information_window("Sparning misslyckades pga. saknad" +
                                       "checkpoint.\n" +
                                       "Det m�ste g� att komma till det " +
                                       "h�gsta nummret fr�n 1.")
                else:
                    unsaved_changes = False
                    save_button.set_greyed_out(True)
                    # [:-3] ignores the .py file ending
                    window.setTitle(window_title + " - " + save_file_name[:-3])

        elif destination_button.is_clicked(mouse_click):
            if current_button != destination_button:
                if current_button:
                    current_button.set_marked(False)
                destination_button.set_marked(True)
                current_tool = set_destination
                current_button = destination_button
            else:
                destination_button.set_marked(False)
                current_tool = None
                current_button = None

        elif blocked_button.is_clicked(mouse_click):
            if current_button != blocked_button:
                if current_button:
                    current_button.set_marked(False)
                blocked_button.set_marked(True)
                current_tool = set_blocked
                current_button = blocked_button
            else:
                blocked_button.set_marked(False)
                current_tool = None
                current_button = None

        elif start_button.is_clicked(mouse_click):
            if current_button != start_button:
                if current_button:
                    current_button.set_marked(False)
                start_button.set_marked(True)
                current_tool = set_start_position
                current_button = start_button
            else:
                start_button.set_marked(False)
                current_tool = None
                current_button = None

        elif delete_button.is_clicked(mouse_click):
            if current_button != delete_button:
                if current_button:
                    current_button.set_marked(False)
                delete_button.set_marked(True)
                current_tool = delete
                current_button = delete_button
            else:
                delete_button.set_marked(False)
                current_tool = None
                current_button = None

        elif exit_button.is_clicked(mouse_click):
            end = True
            if unsaved_changes:
                if confirm_window("Det finns icke sparade �ndringar.\n"+
                                  "Vill du spara f�rst?"):
                    if not save_file_name:
                        save_file_name = get_save_file_name()
                    if not save(tiles, start_position, direction[0],
                                save_file_name):
                        information_window("Sparning misslyckades pga. saknad" +
                                           " checkpoint.\n" +
                                           "Det m�ste g� att komma till det " +
                                           "h�gsta nummret fr�n 1.")
            if end:
                break

        elif current_tool: # Test for click on tiles
            # Gets the coordinates and transforms them to list index
            x = int(mouse_click.getX())-3
            y = int(mouse_click.getY())-3

            if 0 <= x <= 9 and 0 <= y <= 9:
                if current_tool(tiles, start_position, x, y, direction,
                                start_position_mesh):
                    unsaved_changes = True
                    save_button.set_greyed_out(False)
                    save_as_button.set_greyed_out(False)