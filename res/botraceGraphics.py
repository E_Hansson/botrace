# -*- coding: iso-8859-1; -*-

# Original code by Erik Hansson 2014
# Linköpings University

from res.graphics import Point, Rectangle, Text, Circle, Line

BOT_COLOR = ["green", "blue", "yellow", "purple"]
TILE_COLOR = {
    "blocked" : "black",
    "open" : "grey",
    "destination" : "red",
    "visited" : "darkblue"
}

class BotraceTile():

    blocked_square = "black"
    open_square = "grey"
    destination_square = "red"
    visited_square = "darkblue"

    def __init__(self, p1, p2, tile_type):

        self.visited = []
        self.partly = False
        self.text_size = 12
        self.text_color = "white"
        self.tile_type = tile_type


        self.upper_text = Text(Point((p1.getX()+p2.getX())/2,
                                     (p1.getY()+p2.getY())/2+0.25),
                               "")
        self.upper_text.setSize(self.text_size)
        self.upper_text.setTextColor(self.text_color)
        self.lower_text = Text(Point((p1.getX()+p2.getX())/2,
                                      (p1.getY()+p2.getY())/2-0.25),
                                "")
        self.lower_text.setSize(self.text_size)
        self.lower_text.setTextColor(self.text_color)

        self.upper_half = Rectangle(Point(p1.getX(), (p1.getY()+p2.getY())/2),
                                    p2)
        self.upper_half.setWidth(0)
        self.lower_half = Rectangle(p1,
                                    Point(p2.getX(), (p1.getY()+p2.getY())/2))
        self.lower_half.setWidth(0)

        # Paints the square according to the tile type
        if not tile_type:
            self.upper_half.setFill(TILE_COLOR["open"])
            self.lower_half.setFill(TILE_COLOR["open"])
        elif tile_type[0] < 0:
            self.upper_half.setFill(TILE_COLOR["blocked"])
            self.lower_half.setFill(TILE_COLOR["blocked"])
        else:
            self.upper_half.setFill(TILE_COLOR["destination"])
            self.lower_half.setFill(TILE_COLOR["destination"])

        self._update_text()

    def _update_text(self):
        """
        Updates the text of both of the textboxes
        """
        max_numbers = 2
        text = ""
        if self.visited:
            i = 0
            for n in self.visited:
                text += str(n)
                i += 1
                if i == max_numbers:
                    text += "..."
                    break
                elif n != self.visited[-1]:
                    text += ", "
        self.upper_text.setText(text)

        text = ""
        if self.tile_type:
            i = 0
            for n in self.tile_type:
                if n > 0:
                    text += str(n)
                    i += 1
                    if i == max_numbers:
                        text += "..."
                        break
                    elif n != self.tile_type[-1]:
                        text += ", "
        self.lower_text.setText(text)

    def draw(self, win):
        """
        Draw the map in one of the squares
        """
        self.window = win
        self.upper_half.draw(win)
        self.lower_half.draw(win)
        self.upper_text.draw(win)
        self.lower_text.draw(win)

    def visit(self, step_nr, next_target):
        """
        Updates the map as if it was visited
        """
        self.upper_half.setFill(TILE_COLOR["visited"])
        if self.tile_type and self.tile_type[0] == next_target:
            self.tile_type.remove(next_target)
        if not self.tile_type:
            self.lower_half.setFill(TILE_COLOR["visited"])
        self.visited.insert(0, step_nr)

        self._update_text()
        self.window.flush()

class BotraceBotMesh():

    movement_table = {0: [0, 1], 1: [1, 0], 2: [0, -1], 3: [-1, 0]}

    graphics_table = {
        False : { # Multiplayer False
            "C_r" : 0.5,
            0 : {
                "x" : {
                    "high" : 3,
                    "low" : 2,
                },
                "y" : {
                    "high" : 3,
                    "low" : 2,
                }
            }
        },
        True : { # Multiplayer True
            "C_r" : 0.25,
            0 : {
                "x" : {
                    "high" : 2.5,
                    "low" : 2,
                },
                "y" : {
                    "high" : 3,
                    "low" : 2.5,
                }
            },
            1 : {
                "x" : {
                    "high" : 3,
                    "low" : 2.5
                },
                "y" : {
                    "high" : 3,
                    "low" : 2.5
                }
            },
            2 : {
                "x" : {
                    "high" : 3,
                    "low" : 2.5
                },
                "y" : {
                    "high" : 2.5,
                    "low" : 2,
                }
            },
            3 : {
                "x" : {
                    "high" : 2.5,
                    "low" : 2
                },
                "y" : {
                    "high" : 2.5,
                    "low" : 2
                }
            }
        }
    }

    def __init__(self, position, direction, bot_nr, multiplayer=False):

        self.win = None
        self.bot_direction = []
        self.current_direction = direction
        # Draws all the possible direction arrows since there is no way to turn
        # them around with the graphics package
        high_x = self.graphics_table[multiplayer][bot_nr]["x"]["high"]
        low_x = self.graphics_table[multiplayer][bot_nr]["x"]["low"]
        high_y = self.graphics_table[multiplayer][bot_nr]["y"]["high"]
        low_y = self.graphics_table[multiplayer][bot_nr]["y"]["low"]
        self.bot_direction.append(Line(Point(position["x"]+(high_x+low_x)/2,
                                             position["y"]+high_y),
                                       Point(position["x"]+(high_x+low_x)/2,
                                             position["y"]+low_y)))
        self.bot_direction.append(Line(Point(position["x"]+high_x,
                                             position["y"]+(high_y+low_y)/2),
                                       Point(position["x"]+low_x,
                                             position["y"]+(high_y+low_y)/2)))
        self.bot_direction.append(Line(Point(position["x"]+(high_x+low_x)/2,
                                             position["y"]+low_y),
                                       Point(position["x"]+(high_x+low_x)/2,
                                             position["y"]+high_y)))
        self.bot_direction.append(Line(Point(position["x"]+low_x,
                                             position["y"]+(high_y+low_y)/2),
                                       Point(position["x"]+high_x,
                                             position["y"]+(high_y+low_y)/2)))
        for line in self.bot_direction:
            line.setArrow("first")

        radius = self.graphics_table[multiplayer]["C_r"]
        self.bot_background = Circle(Point(position["x"]+(high_x+low_x)/2,
                                           position["y"]+(high_y+low_y)/2),
                                     radius)
        self.bot_background.setFill(BOT_COLOR[bot_nr])

    def draw(self, win):
        """
        Draws the bot in the give window
        """
        self.win = win
        self.bot_background.draw(self.win)
        self.bot_direction[self.current_direction].draw(self.win)
        self.win.flush()

    def move(self, move_dir):
        """
        Moves the bot one step in the given direction
        """
        self.bot_background.move(
            self.movement_table[move_dir][0],
            self.movement_table[move_dir][1]
        )
        for line in self.bot_direction:
            line.move(
                self.movement_table[move_dir][0],
                self.movement_table[move_dir][1]
            )
        self.win.flush()

    def change_direction(self, direction):
        """
        Change the driection of the bot with to the give direction
        """
        self.bot_direction[self.current_direction].undraw()
        self.current_direction = direction
        self.bot_direction[self.current_direction].draw(self.win)
        self.win.flush()

class Button():

    def __init__(self, p1, p2, text):
        self.p1 = p1
        self.p2 = p2
        self.background = Rectangle(p1, p2)
        self.text = Text(Point((p1.getX()+p2.getX())/2,
                               (p1.getY()+p2.getY())/2), text)
        self.text.setStyle("bold")
        self.text.setSize(24)

    def draw(self, window):
        """
        Draws the button to the give window
        """
        self.background.draw(window)
        self.text.draw(window)
        self.window = window

    def is_clicked(self, p):
        """
        Test wether the given point is inside the button
        """
        if self.p1.getX() <= p.getX() <= self.p2.getX() and \
           self.p1.getY() <= p.getY() <= self.p2.getY():
            return True
        else:
            return False

    def set_greyed_out(self, truth):
        """
        sets the button to be greyed out
        """
        if truth:
            self.background.setFill("lightgrey")
        else:
            self.background.setFill("white")
        self.window.flush()

    def set_marked(self, truth):
        """
        Sets the button to be marked
        """
        if truth:
            self.background.setFill("lightblue")
        else:
            self.background.setFill("white")