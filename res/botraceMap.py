# -*- coding: iso-8859-1; -*-

# Original code by Erik Hansson 2014
# Linköpings University

from res.botraceGraphics import BotraceTile
from res.graphics import Point, Line
from copy import deepcopy

class BotraceMap():

    def __init__(self, window, map_file):

        self.window = window

        # Loads the map file
        self._map = map_file["map"]
        self.last_nr = map_file["length"]


    def draw(self):
        """
        Draws the map in the window
        """
        # The squares in the map
        squares = []
        for x in range(10):
            squares.append([])
            for y in range(10):
                squares[x].append(BotraceTile(Point(x+3,y+3), Point(x+4,y+4),
                                              deepcopy(self._map[x][y])))
                squares[x][-1].draw(self.window)
        self.graphics = squares

        # The coordinates
        for x in range(3, 14):
            line = Line(Point(x, 3), Point(x, 13))
            line.draw(self.window)
        for y in range(3, 14):
            line = Line(Point(3, y), Point(13, y))
            line.draw(self.window)

    def get_square_info(self, coordinates):
        """
        Gets the information of the square with the given coordinates
        """
        # Transforms the coordinates to list indexes
        coordinates = {"x" : coordinates["x"]-1, "y" : coordinates["y"]-1}

        return self._map[coordinates["x"]][coordinates["y"]]

    def get_x_length(self):
        """
        Gets the length of the map along the x-axis
        """
        return len(self._map)

    def get_y_length(self):
        """
        Gets the length of the map along the y-axis
        """
        return len(self._map[0])

    def visit(self, coordinates, step, current_goal):
        """
        Marks the given square as visited
        """
        # Transforms the coordinates to list indexes
        coordinates = {"x" : coordinates["x"]-1, "y" : coordinates["y"]-1}

        self.graphics[coordinates["x"]][coordinates["y"]].visit(step,
                                                                current_goal)