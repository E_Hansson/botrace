# -*- coding: iso-8859-1; -*-

# Original code by Erik Hansson 2014
# Link�pings University

from res.botraceBot import BotraceBot
from res.botraceMap import BotraceMap
from res.graphics import GraphWin, Point, Text, Image, Circle
from res.botraceGraphics import Button, BOT_COLOR
from time import sleep

CARD_TABLE = {
    "go" : {
        -1 : "res/cards/card_11.gif",
        1 : "res/cards/card_07.gif",
        2 : "res/cards/card_08.gif",
        3 : "res/cards/card_09.gif",
        4 : "res/cards/card_10.gif",
        "default" : "res/cards/card_07.gif"
    },
    "turn" : {
        -1 : "res/cards/card_12.gif",
        1 : "res/cards/card_13.gif",
        2 : "res/cards/card_14.gif",
        "default" : "res/cards/card_12.gif"
    }
}
TEXT_SIZE = 16

class Botrace():

    def __init__(self):

        self.func_table = {
            "go" : self.move_bot,
            "back" : self.move_bot,
            "turn" : self.turn_bot
        }
        self.stepping = True
        self.finished = False
        self.shutdown = False
        self.step_frequency = 2
        self.instruction_message = "Antal utf�rda instruktioner: "
        self.instruction_counter = []
        self.step_message = "Antal utf�rda steg: "
        self.step_counter = []
        self.finish_message = []

        self.player_amount = 1
        self.bots = []

        self.repeat_instruction = False

    def _draw_player_information(self):
        """
        Writes the player information to the screen
        """
        if self.player_amount == 1:
            self.instruction_counter.append(Text(Point(8, 14.5),
                                                 self.instruction_message +
                                                 "0"))
            self.instruction_counter[0].setSize(TEXT_SIZE)
            self.instruction_counter[0].draw(self.window)
            self.finish_message.append(Text(Point(12.9, 14.5),"I m�l"))
            self.finish_message[0].setSize(TEXT_SIZE)
            self.finish_message[0].setTextColor("red")
            self.finish_message[0].setStyle("bold")
            self.step_counter.append(Text(Point(17, 14.5), self.step_message +
                                          "0"))
            self.step_counter[0].setSize(16)
            self.step_counter[0].draw(self.window)
        else:
            for i in range(self.player_amount):
                text = Text(Point(18, 12.5-i*4*0.625), "Bot " + str(i+1))
                text.setSize(TEXT_SIZE)
                text.setStyle("bold")
                text.draw(self.window)
                color = Circle(Point(19, 12.5-i*4*0.625), 0.25)
                color.setFill(BOT_COLOR[i])
                color.draw(self.window)
                self.finish_message.append(Text(Point(20, 12.5-i*4*0.625),
                                                "I m�l"))
                self.finish_message[i].setSize(TEXT_SIZE)
                self.finish_message[i].setTextColor("red")
                self.finish_message[i].setStyle("bold")
                self.instruction_counter.append(Text(Point(18,
                                                           12.5-(i*4+1)*0.625),
                                                     self.instruction_message +
                                                     "0"))
                self.instruction_counter[i].setSize(TEXT_SIZE)
                self.instruction_counter[i].draw(self.window)
                self.step_counter.append(Text(Point(18, 12.5-(i*4+2)*0.625),
                                              self.step_message + "0"))
                self.step_counter[i].setSize(TEXT_SIZE)
                self.step_counter[i].draw(self.window)

    def _draw_window(self):
        """
        Draws a new botrace window and map
        """
        # Draw the window
        self.window = GraphWin('Botrace', 920, 640, autoflush=False)
        self.window.setCoords(0, 0, 23, 16)

        self._draw_player_information()

        # Draw stepping button if it's activated
        if self.stepping:
            self.step_button = Button(Point(3, 0.5), Point(9, 2.5),
                                          "Stega")
            self.step_button.draw(self.window)

        # Draw a abort button
        self.abort_button = Button(Point(15, 0.5), Point(21, 2.5), "Avsluta")
        self.abort_button.draw(self.window)


    def load_map(self, map_file):
        """
        Loads one of the excersice maps
        """
        self._draw_window()
        self.botrace_map = BotraceMap(self.window, map_file)
        for i in range(self.player_amount):
            self.bots.append(BotraceBot(direction = map_file["direction"],
                                        position = map_file["start"],
                                        map = self.botrace_map,
                                        window = self.window,
                                        nr = i,
                                        multiplayer = self.player_amount != 1))

        # Draw the map
        self.botrace_map.draw()
        for i in range(self.player_amount):
            self.bots[i].draw()
        self.window.flush()

    def _initiate_deck(self):
        """
        Initiates the card that is used
        """
        self.deck = []
        for i in reversed(range(0, len(self.bots[0].instructions))):
            card_type = CARD_TABLE[self.bots[0].instructions[i]["cmd"]].get(
                self.bots[0].instructions[i]["arg"],
               CARD_TABLE[self.bots[0].instructions[i]["cmd"]]["default"]
            )
            card = Image(Point(18, 5.725+i), card_type)
            if i < 6:
                card.draw(self.window)
            self.deck.insert(0, card)

    def _update_deck(self):
        """
        Updates the card pile
        """
        for i in range(min(len(self.deck), 6)):
            self.deck[i].undraw()

        last_played_card = self.deck.pop(0)
        if self.repeat_instruction:
            last_played_card.move(0, len(self.deck))
            # If the deck is small the last card has to be drawn as well
            if len(self.deck) < 6:
                last_played_card.draw(self.window)

        for i in reversed(range(len(self.deck))):
            self.deck[i].move(0, -1)
            if i < 6:
                self.deck[i].draw(self.window)

        if self.repeat_instruction:
            self.deck.append(last_played_card)

    def _check_buttons(self):
        """
        Checks the button and set shutdown flag if needed
        """
        if self.stepping:
            while True:
                mouse_click = self.window.getMouse()
                if self.abort_button.is_clicked(mouse_click):
                    self.shutdown = True
                    break
                elif self.step_button.is_clicked(mouse_click):
                    break
        else:
            mouse_click = self.window.checkMouse()
            if mouse_click and self.abort_button.is_clicked(mouse_click):
                self.shutdown = True

    def _update_step_counter(self, nr):
        """
        Increase the step counter with one
        """
        self.bots[nr].step += 1
        self.step_counter[nr].setText(self.step_message +
                                      str(self.bots[nr].step))
        self.window.flush()

    def _update_instruction_counter(self, nr):
        """
        Increase the instruction counter with one
        """
        self.bots[nr].instruction += 1
        self.instruction_counter[nr].setText(self.instruction_message +
                                             str(self.bots[nr].instruction))
        self.window.flush()

    def move_bot(self, length, nr):
        """
        Moves the bot
        """
        for i in range(abs(length)):
            self._update_step_counter(nr)


            # Forward movements
            if length > 0 and not self.bots[nr].obstructed():
                self.bots[nr].move(1)

            # Backward movements
            elif length < 0 and\
                 not self.bots[nr].obstructed(test_direction_mod=2):
                self.bots[nr].move(-1)

            if self.bots[nr].finished:
                break

            # Sleeps between the steps if it's not the last step
            if not i == length-1:
                sleep(1/self.step_frequency)

    def turn_bot(self, direction_change, nr):
        """
        Changes the graphics of the bot
        """
        self._update_step_counter(nr)
        self.bots[nr].direction = (self.bots[nr].direction+direction_change)%4
        self.bots[nr].refresh_direction()

    def _instructions_left(self):
        """
        Tests if any bot has at least one instruction left to execute
        """
        for bot in self.bots:
            if not bot.instructions:
                return False
        return True

    def start(self):
        """
        Runs the loaded Botrace with the given code
        """

        if self.player_amount == 1:
            self._initiate_deck()

        finished_bots = []

        while self._instructions_left and not self.shutdown and\
              len(finished_bots) != len(self.bots):
            for i in range(self.player_amount):
                if not i in finished_bots and self.bots[i].instructions:

                    self._check_buttons()

                    if self.shutdown:
                        break

                    self._update_instruction_counter(i)

                    instruction = self.bots[i].instructions.pop(0)
                    self.func_table[instruction["cmd"]](instruction["arg"], i)

                    if self.repeat_instruction:
                        self.bots[i].instructions.append(instruction)

                    if self.bots[i].finished:
                        self.finish_message[i].draw(self.window)
                        self.window.flush()
                        finished_bots.append(i)

                    if not self.stepping:
                        sleep(1/self.step_frequency)

                    if self.player_amount == 1:
                        self._update_deck()

        if not self.shutdown:
            while not self.abort_button.is_clicked(self.window.getMouse()):
                pass
