# -*- coding: iso-8859-1; -*-

# Original code by Erik Hansson 2014
# Linköpings University

from res.botraceGraphics import BotraceBotMesh

class BotraceBot():

    def __init__(self, direction, position, map, window, nr, multiplayer):

        self.direction = direction
        if position:
            self.position = {"x" : position[0], "y" : position[1]}
        else:
            self.position = {"x" : 0, "y" : 0}
        self.step = 0
        self.instruction = 0
        self.finished = False
        self.nr = nr
        self.instructions = []

        self._multiplayer = multiplayer
        self._window = window
        self._current_goal = 1
        self._map = map

    def draw(self):
        """
        Draws the bot in the window
        """
        self._graphics = BotraceBotMesh(self.position, self.direction, self.nr,
                                        self._multiplayer)
        self._graphics.draw(self._window)

        # Tests if the bot is starting at the first goal
        if self._current_goal in self._map.get_square_info(self.position):
            if not self._multiplayer:
                self._map.visit(self.position, self.step, self._current_goal)
            self._current_goal += 1

    def obstructed(self, test_direction_mod = 0):
        """
        Tests if the bot is obstructed
        """
        test_direction = (test_direction_mod + self.direction)%4

        # Updates the test coordinates according to the direction
        if test_direction == 0:
            test_coordinates = {"x" : self.position["x"],
                                "y" : self.position["y"]+1}
        elif test_direction == 1:
            test_coordinates = {"x" : self.position["x"]+1,
                                "y" : self.position["y"]}
        elif test_direction == 2:
            test_coordinates = {"x" : self.position["x"],
                                "y" : self.position["y"]-1}
        elif test_direction == 3:
            test_coordinates = {"x" : self.position["x"]-1,
                                "y" : self.position["y"]}


        # Tests wheter the square is outside the map
        if test_coordinates["x"] < 1 or test_coordinates["y"] < 1 or\
           test_coordinates["x"] > self._map.get_x_length() or\
           test_coordinates["y"] > self._map.get_y_length():
            return True
        else:
            # Tests if it's blocked
            square_info = self._map.get_square_info(test_coordinates)
            if square_info and\
               self._map.get_square_info(test_coordinates)[0] < 0:
                return True
            # Else valid move
            return False

    def move(self, distance):
        """
        Moves the robot the give distnace
        """
        if not ((distance > 0 and self.obstructed()) or \
                (distance < 0 and self.obstructed(test_direction_mod=2))):

            update_direction = self.direction if distance > 0 else\
                               (self.direction+2)%4
            # Updates bots position
            if update_direction == 0:
                self.position["y"] += 1
            elif update_direction == 1:
                self.position["x"] += 1
            elif update_direction == 2:
                self.position["y"] -= 1
            elif update_direction == 3:
                self.position["x"] -= 1


            # Updates the graphics
            if not self._multiplayer:
                self._map.visit(self.position, self.step, self._current_goal)
            self._graphics.move(update_direction)

            # Updates the new current goal and tests if it has finished
            if self._current_goal in self._map.get_square_info(self.position):
                self._current_goal += 1
                if self._current_goal > self._map.last_nr:
                    self.finished = True

    def refresh_direction(self):
        """
        Updates the graphics of the bot so it's turned in the right direction
        """
        if not self._multiplayer:
            self._map.visit(self.position, self.step, self._current_goal)
        self._graphics.change_direction(self.direction)