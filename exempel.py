#!/usr/bin/env python3

from functionalBotrace import _botrace, set_stepping, set_repeat, load_map,\
    go, back, right, left, turn, start, set_bot_count

set_stepping(True)
set_repeat(True)
set_bot_count(1) # Allowed bot count 1-4
load_map("uppgift1")


########## start of bot code ##########
go(4, bot_nr = 1)
right(bot_nr = 1)
go(2, bot_nr = 1)
right(bot_nr = 1)
go(4, bot_nr = 1)

########## end of bot code ##########
start()