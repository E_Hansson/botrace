from res.botrace import Botrace
from importlib import import_module

# Global botrace used so that functions can call it without having
# it as an argument
_botrace = Botrace()

########## settings functions ##########

def set_stepping(boolean):
    """
    Sets the stepping flag to boolean
    """
    _botrace.stepping = boolean

def set_repeat(boolean):
    """
    Sets the repeat instruction flag to boolean
    """
    _botrace.repeat_instruction = boolean

def load_map(map_name):
    """
    Loads the given botrace map
    """
    try:
        botrace_map = import_module("Botrace_Maps."+map_name)
    except:
        print("Det finns inte någon bana med namnet: \"" + map_name + "\"")
        exit(1)
    _botrace.load_map(botrace_map.botrace_map)

def set_bot_count(n):
    """
    Sets how many bots there is in the game
    """
    _botrace.player_amount = n

########## primitives ##########

def go(distance, bot_nr=1):
    _botrace.bots[bot_nr-1].instructions.append({"cmd" : "go", "arg": distance})

def back(distance, bot_nr=1):
    _botrace.bots[bot_nr-1].instructions.append({"cmd" : "go",
                                                 "arg" : -distance})

def left(bot_nr=1):
    _botrace.bots[bot_nr-1].instructions.append({"cmd" : "turn" , "arg" : -1})

def right(bot_nr=1):
    _botrace.bots[bot_nr-1].instructions.append({"cmd" : "turn", "arg" : 1})

def turn(bot_nr=1):
    _botrace.bots[bot_nr-1].instructions.append({"cmd" : "turn", "arg" : 2})

######### System commands ##########

def start():
    _botrace.start()
